(function () {
    const button = document.querySelector('button');

    function renderTable(users) {
        const table = `
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Imię i nazwisko</th>
                        <th>Nazwa użytkowanika</th>
                        <th>Adres e-mail</th>
                    </tr>
                </thead>
                <tbody>
                    ${genRows(users)}
                </tbody>
            </table>
        `;

        const div = document.createElement('div');
        div.innerHTML = table;
        document.querySelector('.container').append(div);
    }

    function genRows(users) {
        return users
            .map(
                (user) =>
                    `
                <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                    <td>${user.username}</td>
                    <td><a href="mailto:${user.email}">${user.email}</a></td>
                </tr>

            `,
            )
            .join('');
    }

    button.addEventListener('click', () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then((res) => res.json())
            .then((data) => renderTable(data))
            .then(() => {
                button.remove();
            });
    });
})();
